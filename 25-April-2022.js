//promises
//are nothing but the objects which will return something if the async operation succeeds and if it rejects something.

//three stages, pending, fulfilled and rejected.

// let myPromise = new Promise((resolve, reject) => {
//     // resolve(5);
//     reject(new Error("Delibrate Error"));
// });

//resolve and reject are two call back functions;

// let returnedPromise = myPromise
// .then((value) => {
//     return value // returning will be back to pending state, coz you are not utilising it
// }, (error) => {
//     return(error.message) // returning will be back to pending state
// })

// returnedPromise
// .then((value) => {
//     console.log(value);
// }, (error) => {
//     console.log(error.message)
// })

//A promise is termed as settled once it is either fulfilled or rejected
//but not in pending

//consider i have three console logs, i want to log execution to be
//completed once after the previous one was executed.

// console.log("Hello, i am first console log");

// setTimeout(() => {  
//     console.log("Hello, i am second");
// }, 1000);

// console.log("Hello, i am third console log");
//Hello, i am first console log
// Hello, i am third console log
// Hello, i am second
//here the third is not waiting for the second, it is running as per the stack.


//one way to control this is to use callbacks

// function first() {
//     console.log("Hello, i am first console.log");
//     function second() {
//         setTimeout(() => {
//             console.log("Hello, i am second log");
//             function third() {
//                 setTimeout(() => {
//                     console.log("Hello, i  am third log");
//                     function fourth() {
//                         setTimeout(() => {
//                             console.log("Hello, I am fourth log");
//                             function fifth() {
//                                 console.log("Hello, I am fifth log")
//                             }
//                             fifth();
//                         }, 1000);
//                     }
//                     fourth();
//                 }, 1000)
//             }   
//             third();
//         }, 1000);
//     }
//     second()
// }

// first();

//Hello, i am first console.log
// Hello, i am second log
// Hello, i  am third log
// Hello, I am fourth log
// Hello, I am fifth log

//Cool, it is working with functions calling function calling other functions,
//but what if we have hundreds of tasks which depend on each other,
//it will work but it results in a pyramid of doom structure.
//popularly known as callback hell, which results in difficulty while debugging.

//Solving the same solution using promises.

let myPromise = Promise.resolve();

myPromise
.then(() => {
     return new Promise((resolve, reject) => {
        resolve("Hello, I am first log")
     }); 
})
.then((value) => {
    console.log(value);
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Hello, I am second log");
        }, 1000);
    })
})
.then((value) => {
    console.log(value);
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Hello, I am Third log");
        }, 1000);
    })
    
})
.then((value) => {
    console.log(value);
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Hello, I am Fourth log");
        }, 1000);
    })
})
.then((value) => {
    console.log(value);
    return ("Hello, I am fifth log")
})
.then((value) => {
    console.log(value);
})
// Hello, I am first log
// Hello, I am second log
// Hello, I am Third log
// Hello, I am Fourth log
// Hello, I am fifth log

//it logs the same thing as callbacks, in much cleaner way.