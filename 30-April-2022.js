/*
    Problem 1:
    
    Using promises and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

// const fs = require('fs');
// const path = require('path');

// function createDirectory(directoryName) {
//     return new Promise((resolve, reject) => {
//         fs.mkdir(path.join(__dirname, directoryName), { recursive: true }, (error) => {
//             if (error) {
//                 reject(error)
//             }
//             console.log(`${directoryName} directory created successfully`);
//             resolve(directoryName);
//         })
//     })
// }

// function createFilesInDirectory(directoryName, fileNameWithExt) {
//     return new Promise((resolve, reject) => {
//         fs.writeFile(path.join(__dirname, directoryName, fileNameWithExt), "hello", (error) => {
//             if (error) {
//                 reject(error);
//             }
//             console.log(`${fileNameWithExt} file created in ${directoryName} directory`)
//             resolve(directoryName);
//         })
//     })
// }


// function readDirectory(directoryName) {
//     return new Promise((resolve, reject) => {
//         fs.readdir(path.join(__dirname, directoryName), "utf8", (error, files) => {
//             if(error) {
//                 reject(error);
//             }
//             console.log(`${directoryName} read successfully`);
//             resolve([directoryName,files]);
//         })
//     })
    
// }

// function deleteFilesInDirectory(directoryName, filesArray) {
//     return new Promise((resolve, reject) => {
//         filesArray.map((file) => {
//             fs.unlink(path.join(__dirname, directoryName, file), (error) => {
//                 if(error) {
//                     reject(error);
//                 }
//                 console.log(`${file} file removed from ${directoryName}`);
//             })
//         })
//     })
    
// }

// createDirectory("dir1")//returning promise wrapped with value
//     .then((directory) => {
//         return (createFilesInDirectory(directory, "file1.js"));
//     })
//     .then((directory) => {
//         return createFilesInDirectory(directory, "file2.js");
//     })
//     .then((directory) => {
//         return createFilesInDirectory(directory, "file3.js");
//     })
//     .then((directory) => {
//         return createFilesInDirectory(directory, "file4.js");
//     })
//     .then((directory) => {
//         return createFilesInDirectory(directory, "file5.js");
//     })
//     .then((directory) => {
//         return readDirectory(directory);
//     })
//     .then((pathfileArray) => {
//         deleteFilesInDirectory(pathfileArray[0], pathfileArray[1]);
//     })
//     .catch((error) => {
//         console.log(error);
//     })

/*
    Problem 2:
    
    Using promises and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

// const fs = require("fs");
// const path = require("path");

// function readFile(fileName) {
//     return new Promise((resolve, reject) => {
//         fs.readFile(path.join(__dirname, fileName), "utf8", (error, data) => {
//             if(error) {
//                 reject(error);
//             }
//             resolve(data);
//         })
//     })
// }

// function ToUppercaseAndWriteFile(data, fileNameWithExt) {
//     return new Promise((resolve, reject) => {
//         fs.writeFile(path.join(__dirname, fileNameWithExt), data.toUpperCase(), "utf8", (error) => {
//             if(error) {
//                 reject(error);
//             }
//             console.log(`${fileNameWithExt} is created and data added to it.`);
//             resolve(fileNameWithExt);
//         })
//     })
// }


// readFile("lipsum.txt")
//     .then((data) => {
//         return ToUppercaseAndWriteFile(data, "uppercasedata.js");
//     })
//     .then((fileName) => {
//         return readFile(fileName); //reading and returning the uppercase data
//     })
//     .then((data) => {

//     })