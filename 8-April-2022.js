//Array Utility Methods

//Map
//runs callback for each value in an array and returns the value to new Array based on 
//the testing function.

// let arr1 = [1,2,3,4,5];
// let arr2 = arr1.map((value) => {
//     return value * 2;
// })

// console.log(arr2);

//reformatting objects
// let arr1 = [{key :1,value : 10}, {key : 2, value : 20}, {key : 3, value : 30}];

// let arr2 = arr1.map(({key, value}) => {
//     return {[key] : value}; //we have to use bracket notation to use value of that key
// })

// console.log(arr2);

// let arr1 = ['1','2','3'];
// // let arr2 = arr1.map((element) => parseInt(element, 10));
// // console.log(arr2);

// let arr2 = arr1.map(Number);
// console.log(arr2);

//console.log(Array.of(7));
//basically returns the array taking number of arguments

//console.log(Array(17)); //creates empty slots in array when integer is passed

//console.log(Array.of("sanith", 1,22,true, 55.6));
//console.log(Array("sanith", 1,22,true, 55.6))

//Array.pop returns last element and changes the length of the array
//let arr1 = [1,2,3,4,5];
// let removedValue = arr1.pop();
// console.log(removedValue);
// console.log(arr1);


// let arr1 = [];
// arr1.pop();
// console.log(arr1);

//Array.push
//returns new length of the array
// let arr1 = [1,2,3,4];
// let length = arr1.push(5);
// console.log(arr1);
// console.log(length);

// let arr1 = [1,2,3,4];
// let arr2 = [5,6,7,8];
// //using spread operating passing mutliple args at once

// let newLength = arr1.push(...arr2); //passing iterable spread operator.
// console.log(newLength);
// console.log(arr1);

//REST operator to make copy of the array right away

// let arr1 = [1,2,3];
// let arr2 = [...arr1]; //passed by value

// arr2.push(4);
// console.log(arr1); //remains unaffected
// console.log(arr2); // changed to new array [1,2,3,4]

//reduce

//let arr1 = [1,2,3,4,5,6];

// let sumOfArray = arr1.reduce((previousValue, currentValue) => {
//     return previousValue + currentValue;
// }, 10)

// console.log(sumOfArray);

//seperating even and odd numbers;

// let evenAndOdd = arr1.reduce((previousValue, currentValue) => {
//     if(currentValue%2 === 0) {
//         previousValue.even += currentValue;
//     }else {
//         previousValue.odd += currentValue;//coz iterating over currentValue
//     }
//     return previousValue;
// }, {even: 0, odd: 0})

// console.log(evenAndOdd);

// let arr1 = [{value : 1}, {value : 2}, {value : 3}];

// let sumOfValues = arr1.reduce((previousValue, currentValue) => {
//     return previousValue + currentValue['value'];
// }, 0)

// console.log(sumOfValues);

//we must supply the initial value

// let arr1 = [[1,2], [3,4], [5,6]];

// let flattenArray = arr1.reduce((previousValue, currentValue) => {
//     console.log(previousValue);
//     console.log(previousValue.concat(currentValue));
//     return previousValue.concat(currentValue);
// });

// console.log(flattenArray);

// friends - an array of objects
// where object field "books" is a list of favorite books
const friends = [{
    name: 'Anna',
    books: ['Bible', 'Harry Potter'],
    age: 21
}, {
    name: 'Bob',
    books: ['War and peace', 'Romeo and Juliet'],
    age: 26
}, {
    name: 'Alice',
    books: ['The Lord of the Rings', 'The Shining'],
    age: 18
}]

// allbooks - list which will contain all friends' books +
// additional list contained in initialValue
let allbooks = friends.reduce(function (previousValue, currentValue) {
    //console.log(...previousValue);
    console.log(...currentValue.books);
    return [...previousValue, ...currentValue.books]
}, ['Alphabet'])

  //console.log(allbooks);
  // allbooks = [
  //   'Alphabet', 'Bible', 'Harry Potter', 'War and peace',
  //   'Romeo and Juliet', 'The Lord of the Rings',
  //   'The Shining'
  // ]

