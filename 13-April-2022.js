//Array splice.
// let arr1 = [1,2,3,4,5];

//deleting elements
// arr1.splice(0,1); //start index, no of items to be deleted from that start index
// console.log(arr1);

// let value = arr1.splice(1,2);
// console.log(value); // returns array of removed items //[2,3]
// console.log(arr1);//[1,4,5]

//replace items

// arr1.splice(0,2,1);//removing two elements from index0 and replacing them with 1, the third parameter
// console.log(arr1);

// let returnedArray = arr1.splice();
// console.log(returnedArray); //returned empty array since no items are deleted.

let myFish = ['angel', 'clown', 'mandarin', 'sturgeon']
//Remove 0 (zero) elements before index 2, and insert "drum"

// myFish.splice(2,0, "drum");
// console.log(myFish); //not removing any elements and inserted the drum and inserted drum at index 2

//also adding elements without removing or adding more than removal elemenets will change the array length


//Remove 0 (zero) elements before index 2, and insert "drum" and "guitar"
// myFish.splice(2,0,"drum", "guitar");
// console.log(myFish);

//Remove 1 element at index 3
// myFish.splice(3,1); // logs the removed element
// console.log(myFish);

//Remove 1 element at index 2, and insert "trumpet"
// myFish.splice(2,1,"trumpet");
// console.log(myFish);

//Remove 2 elements from index 0, and insert "parrot", "anemone" and "blue"
// myFish.splice(0,2,"parrot", "anemone", "blue");
// console.log(myFish);

//Remove 2 elements, starting from index 2
// myFish.splice(2,2);
// console.log(myFish);

//Remove 1 element from index -2
// myFish.splice(-2,1);
// console.log(myFish); //[ 'angel', 'clown', 'sturgeon' ]

//Remove all elements, starting from index 2
// myFish.splice(2);
// console.log(myFish); //if we do not mention the delete count all the elements starting from the mentioned index will be removed.

// let arr1 = [1,2,3,4,5, [1,2],{name: "sanith"}];
// console.log(arr1.toString());

//Array unshift

// let arr1 = [1,2,3,4,5];
// //basically it adds one or more elements to the start and returns the new length of the array

// let newLength = arr1.unshift(6,7,8,9);
// console.log(newLength);
// console.log(arr1);


//String methods

//charat

//self explainatory, logs the string at the mentioned index

// let string1 = "Hello My name is sanith!";
// console.log(string1.charAt(6));

//charcodeat
//logs the unicode value at that index

// let string1 = "Hello My name is sanith!";
// console.log(string1.charCodeAt(0))

//string-concat, concat any number of string, displays in order.

// let str1 = "*************";
// let str2 = "END";
// let str3 = "**************";

// console.log(str1.concat(str2,str3));

//EndsWith
//returns boolean value if string ends with the characters you mention

// let str1 = "sanith.garipally@gmail.com";
// let str2 = "sanith.garipally@mountblue.tech";

// console.log(str1.endsWith(".com"));
// console.log(str1.endsWith(".tech"));
// console.log(str2.endsWith(".com"))
// console.log(str2.endsWith(".tech"))

//fromcharcode give the value at mentioned unicode;

// console.log(String.fromCharCode(65)); //A
// console.log(String.fromCharCode(90)); //z

//includes
//case-sensitive search, returns boolean value

// let str1 = "My name is sanith!";

// console.log(str1.includes("sanith"));
// console.log(str1.includes("Sanith"));