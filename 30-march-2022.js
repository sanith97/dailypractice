//call by value and call by reference

//by value

let number1 = 2; // points to a memory location
let number2 = number1; // points to a different memory location other than x.

//x is pointing to the address of memory location, if is a primitive data type then y = x,
//the value stored will be copied to y and changing x will not impact y.

number1 = 3;

//console.log(number1);//3 
//console.log(number2);//2


let value1 = 4;
let value2 = value1;

//console.log(value1 == value2);//true since the value is copied 
//by reference for objects,arrays and functions

let array1 = [1,2,3];
let array2 = array1;

array2.push(4);
//console.log(array1);//[ 1, 2, 3, 4 ]
//console.log(array2);//[ 1, 2, 3, 4 ]

let array3 = [1,2,3];
let array4 = [1,2,3];

//console.log(array3 == array4);//false
//console.log(array3 === array4);//false
//why? when we say array3 = array4, it is pointing to the memory locations of the arrays.
const array5 = [1,2,3,4];
const array6 = array5;

array6.push(5);

// console.log(array5);
// console.log(array6);

let object1 = {
    'name' : 'Sanith',
    'designation' : 'Software Engineer',
    'Location' : 'Bangalore'
}

let object2 = object1; // now both are pointing to the same memory address location

object1['Location'] = 'Hyderabad';

// console.log(object1);
// console.log(object2);
//both are same as they are referring to the same memory location.

function callbyreference(number, obj1, obj2) {
    number = number * 10;
    obj1['item'] = 'changed';
    obj2 = {'item' : 'changed'};
}

let number = 10;

let obj1 = {
    'item' : 'unchanged'
};

let obj2 = {
    'item' : 'unchanged'
};

// callbyreference(number,obj1,obj2) //all these arguments are passing by values
// console.log(number); // no change in o/p coz it is passing by value
// console.log(obj1); // changed coz value passed is a reference and it is ponting to same memory location
// console.log(obj2); //assignment has changed, memory location as well.

// size of the primitive value is fixed, so the values are stored in Execution Context.
//objects, functions, arrays - size cannot be determined, so the variables just point
//the address where it is stored. 

const arr1 = [1,2,3,4,5];
arr1.push(6);
