//string IndexOf
//takes one argument "the sub string" which we want to find in the given string and returns the first occurance
//case-sensitive

// let str1 = "hello world, my name is sanith."
// console.log(str1.indexOf("sanith"));//24
// console.log(str1.indexOf("o"));//first occurance in hello, the 4th index
// console.log(str1.indexOf("z"));
//-1 if the specified sub string is not found in the string.

//method also takes second argument as number, which will begin your search from that index

// console.log(str1.indexOf("sanith", 25));// if i mention 25 it starts at 25 and there is no sanith after that so it returns -1



// console.log(str1.indexOf("my"));//19 default first occurance
//what if i want to search after index 20 to check if there is any element
// console.log(str1.indexOf("my", 21));//30, since there is element and we are getting the index of second occurance

//return values

// console.log("hello sanith".indexOf(""));//0
// console.log("Hello india".indexOf(""));//0

//if we provide second argument and search substring is empty string,
//1. if the value passed as argument is lessthan the main string length it returns the number passed

// console.log("Hello India".indexOf("", 11))//11
// console.log("Hello India".indexOf("", 10))//10

//however if it is greater than or equal to the string length, it returns the length of the string.
// console.log("Hello India".indexOf("", 12))

//USE CASE
//simple way to find the number of occurances of a letter in a given string.

let str1 = "hello world, hello my name is my name is sanith, iam from kamareddy. I did my graduation from nagarjuna college of engineering and technology, in the year 2019 from bangalore.";
//i want to count my count in the str1

let count = 0;
let position = str1.indexOf("my");//position stores first occurance

// for (let letter of str1) {
//     if(position !== -1) {
//         count++;
//         position = str1.indexOf("my", position + 1); //after iteration i am changing my search from the next index 
//     } 
//     else {
//         break
//     }
// }
// console.log(count);//3

//we can also do this using while loop, simple 2 liner
// while(position !== -1) {
//     count++;
//     position = str1.indexOf("my", position+1);
// }

// console.log(count);//3

//lastindex works similar but returns the last occurance

// console.log(str1.lastIndexOf("my"));//75
// //second argument a staring index, lessthan or equal to the specified index
// console.log(str1.lastIndexOf("my", 74)); //checks between 0 to 74 and logs the last index found

//pad end and pad start, cool feature

//console.log("end".padEnd(25,"*"));
//take the count of the string to be joined after the specified string and string to be added the mentioned number of times
//console.log("start".padStart(30, "~"));
//console.log("Start".padStart(20, "*") + "".padEnd(20, "*"))

//repeat to create copies

// let greet = 'Hi ';
// let copies = greet.repeat(10); //concatenates the copies form a sequence
// console.log(copies);

//replace
//replace the first occurance of the string with the provided string.
//original array will not be altered instead a clone is returned with the altered value
//case-sensitive

// let greet = "Hello Sanith!";
// let modifiedstring = greet.replace("hello", "Hi");
// console.log(greet);
// console.log(modifiedstring);

//replaceall replaces all occurances and returns new string.
// let greet = "hi hi hi hi hi hi";
// let modifiedString = greet.replaceAll("hi", "Hello");
// console.log(modifiedString);

