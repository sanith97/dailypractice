const fs = require('fs');
const path = require('path');

//CRUD OPERATIONS - BASICS
//__dir and resolve indicates the absolute path of the current folder we are in

//******************create files*****************************
// fs.writeFile(path.resolve("file1.js"), "Hello world!", (error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log('file written successfully');
// })

// fs.writeFile(path.join(__dirname, "file2.js"), "Hello world!", (error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log('file written successfully');

// })

//********************Writing files inside a directory**************************

// fs.writeFile(path.join(__dirname,"/mydir/","file2.js"),"Hello World",(error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log("file created successfully");
// })

// fs.writeFile(path.resolve("mydir/file3.js"),"Hello World",(error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log("file created successfully");
// })

//******************create Directory*****************************

// fs.mkdir(path.resolve("mydir"), (error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log("directory created");
// })

// fs.mkdir(path.join(__dirname,"mydir2"), (error) => {
//     if(error) {
//         console.log(error);
//     }
//     console.log("directory created");
// })

//******************Read Files*****************************

// fs.readFile(path.resolve("file1.js"), "utf8", (error, data) => {
//     if(error) {
//         return console.log(error)
//     }

//     console.log(data);
// })

// fs.readFile(path.join(__dirname, "file1.js"), "utf8", (error, data) => {
//     if(error) {
//         return console.log(error)
//     }

//     console.log(data);
// })


//******************Update Files*****************************

// fs.appendFile(path.resolve("file1.js"), "\nMy name is sanith.", (error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log("File updated successfully");
// })

// fs.readFile(path.resolve("file1.js"), "utf-8", (error, data) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log(data);
// })

// fs.appendFile(path.join(__dirname, "file1.js"), "\nIam from Kamareddy.", (error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log("File updated successfully");
// })

// fs.readFile(path.join(__dirname, "file1.js"), "utf-8", (error, data) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log(data);
// })




//Daily Practice Higher Order Functions

let data = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

// 1. Find all people who are Agender
// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
// 3. Find the sum of all the second components of the ip addresses.
// 3. Find the sum of all the fourth components of the ip addresses.
// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
// 5. Filter out all the .org emails
// 6. Calculate how many .org, .au, .com emails are there
// 7. Sort the data in descending order of first name

let agenderObjects = data.filter((object) => {
    if(object['gender'].includes('Agender')) {
        return true;
    }
});

//console.log(agenderObjects);

let ipComponent = data.map((object) => {
    object['ip_address'] = object['ip_address'].split(".").map((number) => Number(number)); //using split makes it array and using map to assign each value to number type
    return object;
})

//console.log(ipComponent);

let sumOfSecondComponents = data.map((object) => {
    return object['ip_address'][1]; //creating an array using only 2nd components of ip address

}).reduce((firstComponent, secondComponent) => {
    return firstComponent + secondComponent;
})

//console.log(sumOfSecondComponents);

let sumOfFourthComponents = data.map((object) => {
    return object['ip_address'][3]; //creating an array using only 2nd components of ip address

}).reduce((firstComponent, secondComponent) => {
    return firstComponent + secondComponent;
})

//console.log(sumOfFourthComponents);


let fullNameObject = data.map((object) => {
    let firstName = object['first_name'];
    let lastName = object['last_name'];

    let fullName = firstName + " " + lastName;

    object['full_name'] = fullName;

    return object;
})

//console.log(fullNameObject);

let orgEmailObjects = data.filter((object) => {
    if(object['email'].endsWith(".org")) {
        return true;
    }
})

//console.log(orgEmailObjects);


let requiredObjects = data.filter((object) => {
    if(object['email'].endsWith(".org") || object['email'].endsWith(".com") || object['email'].endsWith('.au')) {
        return true;
    }
})

//console.log(requiredObjects.length);

//sorting data by firstname

let sortedObjects = data.sort((object1, object2) => {
    let name1 = object1['first_name'];
    let name2 = object2['first_name'];

    if(name1 > name2) {
        return -1; //for ascendng return 1

    }
    if(name1 < name2) {
        return 1; //for ascendng return -1
    }
    return 0;
})

//console.log(sortedObjects);