//file system CRUD Operations - Asynchronous module
const fs = require("fs");
const path = require("path");

//Creating
//file
// fs.writeFile(path.join(__dirname, "file1.js"), "Hello sanith", "utf8", ((error) => {
//     if(error) {
//         console.log(error);
//     } else {
//         console.log("File created successfully")
//     }
// }))

// resolve-absolute path calculation of the relative path
// fs.writeFile(path.resolve("file4.js"), "Hello world", ((error) => {
//     if(error) {
//         console.log(error);
//     } else {
//         console.log("File4.js created successfully");
//     }
// }))

// console.log(path.resolve("file4.js"));

//creating directory
//__dirname is an environment variable that tells you the absolute path of the directory containing the currently executing file.

// fs.mkdir(path.join(__dirname, "dir1"), (error) => {
//     if(error) {
//         console.log(error);
//     } else {
//         console.log("dir1 created successfully");
//     }
// })

// console.log(path.join(__dirname));

// fs.mkdir(path.resolve("dir2"), ((error) => {
//     if(error) {
//         console.log(error);
//     } else {
//         console.log("dir2 created successfully");
//     }
// }))

// function createFile(fileName, data) {
//     fs.writeFile(path.resolve(fileName), data, "utf-8", ((error) => {
//         if(error) {
//             console.log(error);
//         }
//         else {
//             console.log(`${fileName} file created successfully`);
//         }
//     }))
// }

// module.exports = createFile;

// console.log(path.join());
// console.log(path.resolve());
// console.log(path.join(__filename));

//reading the file
// fs.readFile(path.join(__dirname, "lipsum.txt"), "utf-8", (error, data) => {
//     if(error) {
//         console.log(error);
//     } 
//     else {
//         console.log(data);
//     }
// })

// fs.readFile(path.resolve("lipsum.txt"), "utf8", (error, data) => {
//     if(error) {
//         console.log(error);
//     } else {
//         console.log(data);
//     }
// })

//reading directory
// fs.mkdir(path.resolve("dir1"), {recursive:true}, (error) => {
//     if(error) {
//         console.log(error);
//     } else {
//         console.log("dir1 created successfully");
//     }
// });

// for(let index = 0; index < 5; index++) {
//     fs.writeFile(path.resolve("dir1", `file${index}.js`), "Hello", (error) => {
//         if(error) {
//             console.log(error);
//         } else {
//             console.log("file created successfully");
//         }
//     })
// }

// fs.readdir(path.resolve("dir1"), "utf-8", (error, data) => {
//     if(error) {
//         console.log(error);
//     } else {
//         deleteFiles(data);
//     }
// })

//update existing file

// fs.writeFile(path.resolve("file1.js"), "Hello World", ((error) => {
//     if(error) {
//         console.log(error);
//     } else {
//         console.log("file1.js created successfully");
//     }
// }));

// fs.appendFile(path.resolve("file1.js"), "\n My name is sanith", "utf-8", ((error) => {
//     if(error) {
//         console.log(error);
//     }
//     else {
//         console.log("file1.js data appended successfully");
//     }
// }))

// function deleteFiles(array) {
//     for(let index of array) {
//         fs.unlink(path.join(__dirname,"dir1", index), (error) => {
//             if(error) {
//                 console.log(error);
//             }
//             else {
//                 console.log(`${index} removed successfully`);
//             }
//         })
//     }
// }

// fs.rmdir(path.join(__dirname, "dir1"), (error) => {
//     if(error) {
//         console.log(error);
//     } else {
//         console.log("directory removed successfully");
//         }
// })