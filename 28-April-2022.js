//ES6

// function sayHello() {
//     for(let i = 0; i < 5; i++) {
//         console.log(i);
//     }
//     console.log(i)
// }

// sayHello();

//var - function scoped
//let , const - block scoped

// const x = 1;
// x = 2;
// console.log(x);



// console.log(person['name']);
//functions inside objects are
//called methods 

// const targetPerson = 'name';

// person[targetPerson] = 'ajay'
// console.log(person.name);
// person.walk();
// person.talk();

// person.walk();

// const walk = person.walk.bind("Hello");
// walk();

// let arr1 = [1,2,3,4,5];

// let arr2 = arr1.map((number) => {
//     return number * 2;
// })

// console.log(arr2)

// const person = {
//     talk() {
//         setTimeout(() => {
//             console.log('this' , this);

//         }, 1000)
//     }

// }

// person.talk();

//object destructuring

// const address = {
//     street: "Devi Vihar 3rd lane",
//     city: "Kamareddy",
//     country: "India"
// };

// let {street:colony} = address;

// // console.log(street);
// console.log(colony);

// const array1 = [1,2,3];
// const array2 = [4,5,6];

// // const combined = [...array1, "hello" ,...array1, "sanith"]

// // console.log(combined);

// const clone = [...array1];
// console.log(array1);
// console.log(clone)

// const first = {
//     name: "sanith"
// }

// const second = {
//     designation: "Software Engineer"
// }

// const combine = {...first, ...second, location:"Bangalore"};

// console.log(combine);

// const clone = {...first};
// console.log(first);
// console.log(clone);

// class Person {

//     constructor(name) {
//         this.name = name;
//     }

//     walk(name) {
//         console.log(name)
//     }
    
// }

// const person = new Person("sanith");
// person.walk("sanith");




// import { Teacher } from "./teacher.js";
// const teacher = new Teacher("sanith", "ME");
// teacher.teach();