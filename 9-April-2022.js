//Array utility methods
//reduce,

//without intitial value,

// function reducer(previousValue, currentValue, index, array) {
//     let reducedValue = previousValue + currentValue;
//     console.log(`previousValue - ${previousValue}, currentValue = ${currentValue}, index - ${index}, array - ${array}`);

//     return reducedValue;
// }

// let reducedValue = arr1.reduce(reducer);//calling reducer function for each iteration

// console.log(reducedValue);

//with initial value
// let arr2 = arr1.reduce((previousValue, currentValue) => {
//     return previousValue + currentValue;
// }, 20);
// console.log(arr2);
// Behavior during array mutations
//once reduce begins, making changes to the original array will not impact the array which we are performing the hof

//just to check whether changes are reflecting
// arr1.reduce((previousValue, currentValue, index, array) => {
//     array.push(0);//pass by reference
// })

// console.log(arr1);

// let arr2 = arr1.reduce((previousValue, currentValue, index, array) => {
//     array.push(10);
//     return previousValue + currentValue;
// });

// console.log(arr1);
// console.log(arr2);
// [
//     10, 11, 12, 13, 14,
//     10, 10, 10, 10
//   ]
// 60

//performing any actions on the array which we are performing which not make any differences to the actual result.

// let arr1 = [10,11,12,13,14];
// //what if i make change to an existing element
// let arr2 = arr1.reduce((previousValue, currentValue, index, array) => {
//     array[4] = 13;
//     return previousValue + currentValue;
// })
// console.log(arr1);
// console.log(arr2);
//same thing here, the array was not called on that element yet

// let arr1 = [10,11,12,13,14];
// //what if i make change to an existing element
// let arr2 = arr1.reduce((previousValue, currentValue, index, array) => {
//     array[2] = 13;
//     return previousValue + currentValue;
// })
// console.log(arr1);
// console.log(arr2);

// function checkReduce(array) {
//     console.log(array);
//     array.map((number) => {
//         console.log(number * 2);
//     })
// }

// let arr1 = [1,2,3,4,5];
// arr1[0] = 0;
// checkReduce(arr1); //before calling the function it will take the array even if it is modified
// arr1[0] = 1;
// console.log(arr1);//function already took an array it has the value when you called it.

// let arr1 = [1,2,3,4,5];
// let arr2 = arr1.reduce((previousValue, currentValue, index, array) => {
//     array.splice(2,1);
//     return previousValue + currentValue;
// })

// console.log(arr1);
// console.log(arr2);

//removing duplicates using reduce
// const myArray = ['a', 'b', 'a', 'b', 'c', 'e', 'e', 'c', 'd', 'd', 'd', 'd']
// let myArrayWithNoDuplicates = myArray.reduce(function (previousValue, currentValue) {
//   if (previousValue.indexOf(currentValue) === -1) { //if not present in the array then we are pushing it. else returning it
//     previousValue.push(currentValue)
//   }
//   return previousValue
// }, [])

// //console.log(myArrayWithNoDuplicates)


// let arr1 = [1,2,3,4,5,5,5,3,4,5,6,1,2,3,4,9];

// let removedDuplicatesArray = arr1.reduce((previousValue, currentValue) => {
//     if(previousValue.indexOf(currentValue) === -1) {
//         previousValue.push(currentValue);
//     }
//     return previousValue;
// }, []);

// console.log(removedDuplicatesArray);
// [
//     1, 2, 3, 4,
//     5, 6, 9
// ]

const data = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

// 1. Find all people who are Agender
// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
// 3. Find the sum of all the second components of the ip addresses.
// 3. Find the sum of all the fourth components of the ip addresses.
// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
// 5. Filter out all the .org emails
// 6. Calculate how many .org, .au, .com emails are there
// 7. Sort the data in descending order of first name

data.map((object) => {
    object['ip_address'] = object['ip_address'].split(".")
    return object;
})

let secondComponentSum = data.reduce((previousObject, currentObject)  => {
    previousObject += Number(currentObject['ip_address'][1]);
    
    return previousObject;
}, 0);

//console.log(secondComponentSum);

let fourthComponentSum = data.reduce((previousObject, currentObject) => {
    previousObject += Number(currentObject['ip_address'][3]);
    return previousObject;
}, 0)

//console.log(fourthComponentSum);

let domainsSum = data.reduce((previousObject, currentObject) => {
    if(currentObject['email'].endsWith(".org") || currentObject['email'].endsWith(".au") || currentObject['email'].endsWith(".com")) {
        previousObject += 1;
    }
    return previousObject;
}, 0)

console.log(domainsSum);