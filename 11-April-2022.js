//Array Reverse - destructive, changes the original array

// let arr1 = [1,2,3,4,5];
// arr1.reverse();

// console.log(arr1);


// let arr1 = {
//     0 : 1,
//     1 : 2,
//     2 : 3,
//     length : 3
// }

// Array.prototype.reverse.call(arr1);
// console.log(arr1);


//Shift, removes first elements and returns the element.

// let arr1 = [0,2,3,4,5,6];
// let removedFirstElement = arr1.shift();

// console.log(removedFirstElement);
// console.log(arr1);

// let arr1 = [];

// let removedElement = arr1.shift();

// console.log(removedElement);//undefined
// console.log(arr1);

//Array Slice
//If array includes object and sliced it, changing properties of object in sliced array, will be reflected in original array

// let arr1 = [1,2,3,4,5];

// let arr2 = arr1.slice(-3);

// console.log(arr1);
// console.log(arr2);

// Using slice, create newCar from myCar.
// let myHonda = { color: 'red', wheels: 4, engine: { cylinders: 4, size: 2.2 } }
// let myCar = [myHonda, 2, 'cherry condition', 'purchased 1997']
// let newCar = myCar.slice(0, 2)

// // Display the values of myCar, newCar, and the color of myHonda
// //  referenced from both arrays.
// console.log('myCar = ' + JSON.stringify(myCar))
// console.log('newCar = ' + JSON.stringify(newCar))
// console.log('myCar[0].color = ' + myCar[0].color)
// console.log('newCar[0].color = ' + newCar[0].color)

// // Change the color of myHonda.
// myHonda.color = 'purple'
// console.log('The new color of my Honda is ' + myHonda.color)

// // Display the color of myHonda referenced from both arrays.
// console.log('myCar[0].color = ' + myCar[0].color)
// console.log('newCar[0].color = ' + newCar[0].color)

// let myCar = {
//     company : "TaTa",
//     name : "Nexon",
//     color : "White",
//     model : "BSVI",
//     engine : {
//         cylinders : 8,
//         size : 1222
//     },
//     price : 700000
// }

// let carDetails = [myCar, "Black Edition", "Purchases 2023"];

// let slicedArray = carDetails.slice(0,2);

// console.log(`previous color - ${myCar.color}`);

// myCar.color = "Black",

// console.log(`changed color - ${slicedArray[0].color}`);
// console.log(`original color changed to - ${carDetails[0].color}`);

//Array-some
//returns true atleast one element matches with the testing function

// let arr1 = [1,2,3,4,5,6];

// let arr2 = arr1.some((value) => {
//     return value % 2 === 0;
// })

// console.log(arr2);

//Array -Sort, by default ascending

//1. if compare function is not passed, sort takes every element in the form of string and compares the unicodes of them

// const months = ['March', 'Jan', 'Feb', 'Dec', 'march'];
// months.sort();
// console.log(months);
// // expected output: Array ["Dec", "Feb", "Jan", "March"]

// const array1 = [1, 30, 4, 21, 100000];
// array1.sort();
// console.log(array1);
// // expected output: Array [1, 100000, 21, 30, 4]

// let arr1 = [42345, 3456, 10000, undefined, true, 3.16];
// arr1.sort(); //compare function not supplied so the sort is considering the elements as strings

// console.log(arr1);

//a, b, > 0, sorts b before a, a- b let arr1 = [42345, 3456, 10000, undefined, true, 3.16];
// arr1.sort(); //compare function not supplied so the sort is considering the elements as strings

// console.log(arr1);


// let numArray = [10,1,2,4,5,6,7,8,12];

// numArray.sort((previousNumber, currentNumber) => {
//     return previousNumber - currentNumber;
// })

// console.log(numArray);

//sort by value
// const items = [
//     { name: 'Edward', value: 21 },
//     { name: 'Sharpe', value: 37 },
//     { name: 'And', value: 45 },
//     { name: 'The', value: -12 },
//     { name: 'Magnetic', value: 13 },
//     { name: 'Zeros', value: 37 }
//   ];
  

// items.sort((previousObject, currentObject) => {
//     return previousObject.value - currentObject.value;
// })  

// //sorting using names

// items.sort((previousName, currentName) => {
//     let name1 = previousName.name.toLowerCase();
//     let name2 = currentName.name.toLowerCase();

//     if(name1 > name2) {
//         return 1;
//     }
//     else if(name1 < name2) {
//         return -1;
//     }
//     else {
//         return 0;
//     }
// })

// console.log(items);

