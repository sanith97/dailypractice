// <!DOCTYPE html>
// <html>
//   <head>
//     <meta charset="utf-8" />
//     <meta name="viewport" content="width=device-width" />
//     <meta name="description" content="Affordable and professional web design" />
//     <meta
//       name="keywords"
//       content="web design, affordable web design, professional web design"
//     />
//     <meta name="author" content="Sanith Garipally" />
//     <title>Acme Web Design | Welcome</title>
//     <link rel="stylesheet" href="./css/style.css" />
//   </head>
//   <body>
//      <header>
//          <div class="container">
//             <div id="branding">
//                 <h1><span class="highlight">Acme</span>Web Design</h1>
//             </div>
//             <nav>
//                 <ul>
//                     <li class="current"><a href="index.html">HOME</a></li>
//                     <li><a href="about.html">ABOUT</a></li>
//                     <li><a href="services.html">SERVICES</a></li>
//                 </ul>
//             </nav>
//          </div>
//      </header>
     
//      <section id="showcase">
//         <div class="container">
//             <h1>Affordable Professional Websites</h1>
//             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus mollis efficitur. Vestibulum vestibulum, lectus at semper mattis, lacus lorem rutrum arcu, et porttitor nulla eros in odio. Aenean malesuada nibh sed semper placerat. Donec auctor gravida metus in interdum.</p>
//         </div>
//      </section>

//      <section id="newsletter">
//         <div class="container">
//             <h1>Subscribe To Our Newsletter</h1>
//             <form>
//                 <input type="email" placeholder="Enter Email...">
//                 <button type="submit" class="button_1">Subscribe</button>
//             </form>
//         </div>
//      </section>

//      <section id="boxes">
//         <div class="container">
//             <div class="box">
//                 <img src="./img/html5-brands.svg">
//                 <h3>HTML5 Markup</h3>
//                 <p>
//                     Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus mollis efficitur. Vestibulum vestibulum, lectus at semper mattis, lacus lorem rutrum arcu, et porttitor nulla eros in odio.
//                 </p>
//             </div>
//             <div class="box">
//                 <img src="./img/css3-alt-brands.svg">
//                 <h3>CSS3 Styling</h3>
//                 <p>
//                     Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus mollis efficitur. Vestibulum vestibulum, lectus at semper mattis, lacus lorem rutrum arcu, et porttitor nulla eros in odio.
//                 </p>
//             </div>
//             <div class="box">
//                 <img src="./img/laptop-code-solid.svg">
//                 <h3>Security</h3>
//                 <p>
//                     Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus mollis efficitur. Vestibulum vestibulum, lectus at semper mattis, lacus lorem rutrum arcu, et porttitor nulla eros in odio.
//                 </p>
//             </div>
//         </div>
//      </section>

//      <footer>
//          <p>Acme Web Design, Copyright &copy; 2022</p>
//      </footer>
//      <button>Get Users</button>
//      <script>
//         //  console.log("Hello, How you doing?")
//         //  console.error("This is an error");
//         //  console.warn("This is a warning");
//         //  console.dir(document)
//         // console.table([{name:"Sanith", place: "bangalore", company: "mountblue"}])
//         // console.group("My group");
//         //     console.log("Hello ajay");
//         //     console.log("Hello bhaskar");
//         //     console.log("Hello shreyas");
//         //     console.log("Hello sagar");
//         //     console.log("Hello vamsi");
//         // console.groupEnd("Group Ends")

//         // function isgreater(x,y) {
//         //     console.assert(x=y, {'message': 'x is not greater than y', "x":x,'y':y});
//         // }
//         // isgreater(4,5);
//         // document.querySelector("button").addEventListener("click", getUsers);

//         // function getUsers() {
//         //     const xhr = new XMLHttpRequest();
//         //     xhr.open('GET', "https://reqres.in/api/products/3", true);

//         //     xhr.onload = function() {
//         //         if(this.status== 200) {
//         //             console.log("Users Returned");
//         //         }
//         //     }
//         //     xhr.send();
//         // }

//         localStorage.setItem('name', 'sanith');
//         // console.log(localStorage.getItem('name'));
//         // localStorage.clear("name");
//         sessionStorage.setItem('name', "ajay");
//         console.log(sessionStorage.getItem("name"));
//         sessionStorage.clear("name");
//      </script>
//   </body>
// </html>

// CSS RuleSets

// body {
//     font: 15px 1.5 Arial, Helvetica, sans-serif;
//     padding: 0;
//     margin: 0;
//     background-color: #f4f4f4;
// }

// /* Global */
// .container {
//     width: 80%;
//     margin: auto;
//     overflow: hidden;
// }

// ul {
//     margin: 0;
//     padding: 0;
// }

// .button_1 {
//     height: 38px;
//     background: #e8491d;
//     border: 0;
//     padding-left: 20px;
//     padding-right: 20px;
//     color: #fff;
// }

// .dark {
//     padding: 15px;
//     background: #35424a;
//     color: #fff;
//     margin-top: 10px;
//     margin-bottom: 10px;
// }

// /* Header */

// header {
//     background-color: #35424a;
//     color: #fff;
//     padding-top: 30px;
//     min-height: 70px;
//     border-bottom: #e8491d 3px solid;
// }

// header a {
//     color: #fff;
//     text-decoration: none;
//     text-transform: uppercase;
//     font-size: 16px;
// }

// header li {
//     float: left;
//     display: inline;
//     padding: 0 20px 0 20px;
// }

// header #branding {
//     float: left;
// }

// header #branding h1 {
//     margin: 0;
// }

// header nav {
//     float: right;
//     margin-top: 10px;
// }

// header .highlight, header .current a {
//     color: #e8491d;
//     font-weight: bold;

// }

// header a:hover {
//     color: #cccccc;
//     font-weight: bold;
// }

// /* Showcase */

// #showcase {
//     min-height: 400px;
//     background: black;
//     text-align: center;
//     color: #fff;
// }

// #showcase h1 {
//     margin-top: 100px;
//     font-size: 55px;
//     margin-bottom: 10px;   
// }

// #showcase p {
//     font-size: 20px;
// }

// /*boxes*/

// #newsletter {
//     padding: 15px;
//     color: #fff;
//     background: #35424a;
// }

// #newsletter h1 {
//     float: left;
// }

// #newsletter form {
//     float: right;
//     margin-top: 15px;
// }

// #newsletter input[type="email"]{
//     padding: 4px;
//     height: 25px;
//     width: 250px;
// }

// /* boxes */
// #boxes {
//     margin-top: 20px;
// }

// #boxes .box {
//     float: left;
//     text-align: center;
//     width: 30%;
//     padding: 10px;
// }

// #boxes .box img {
//     width: 90px;
// }

// /* sidebar */
// aside#sidebar {
//     float: right;
//     width: 30%;
//     margin-top: 10px;
// }

// aside#sidebar .quote input, aside#sidebar .quote textarea {
//     width: 90%;
//     padding: 5px;
// }

// /* main-col */
// article#main-col {
//     float: left;
//     width: 65%;
// }

// /* services */
// ul#services li {
//     list-style: none;
//     padding: 20px;
//     border: #cccccc solid 1px;
//     margin-bottom: 5px;
//     background: #e6e6e6;
// }


// footer {
//     padding: 20px;
//     margin-top: 20px;
//     color: #fff;
//     background-color: #e8491d;
//     text-align: center;
// }

// /* Media Queries */
// @media(max-width: 768px) {
//     header #branding,
//     header nav,
//     header nav li,
//     #newsletter h1,
//     #newsletter form,
//     #boxes .box,
//     article#main-col,
//     aside#sidebar {
//         float: none;
//         text-align: center;
//         width: 100%;
//     }

//     header{
//         padding-bottom: 20px;
//     }

//     #showcase h1 {
//         margin: 40px;

//     }

//     #newsletter button, .quote button {
//         display: block;
//         width: 100%;
//     }

//     #newsletter form input[type="email"], .quote input, .quote textarea {
//         width: 100%;
//         margin-bottom: 5px;
//     }
// }
