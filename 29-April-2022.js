const fs = require('fs');
const path = require('path');

// FS CRUD Operations

//Create File

// fs.writeFile(path.join(__dirname, "file1.js"), "Hello world", (error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log("file created successfully");
// })

// fs.writeFile(path.resolve(".", "file2.js"), "Hello World", (error) => {
//     if(error) {
//         return console.log(error);
//     }
//     console.log("File created successfully using resolve method");

// })


//****************** creating directory */

// fs.mkdir(path.join(__dirname, "mydir"), { recursive : true }, (error) => {
//     if(error) {
//         return console.log(error);
//     }
//     console.log("Directory created using join method");
// })

// fs.mkdir(path.resolve(".", "mydir2"), { recursive : true }, (error) => {
//     if(error) {
//         return console.log(error);
//     }
//     console.log("Directory created using resolve method");
// })


//**************** reading files **********/ 

// fs.readFile(path.join(__dirname, "file1.js"), "utf8", (error, data) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log(data);
// })

// fs.readFile(path.resolve(".", "file2.js"), "utf8", (error, data) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log(data);
// })

// fs.appendFile(path.join(__dirname, "file1.js"), "\nMy name is sanith", (error) => {
//     if(error){
//         return console.log(error);
//     }

//     console.log("file appended successfully");
// })

// fs.readFile(path.join(__dirname, "file1.js"), "utf8", (error, data) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log(data);
// })

// fs.unlink(path.join(__dirname, "file2.js"), (error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log("File removed successfully");
// })


//************* Write files in directory */

// function createFiles() {

//     for(let index = 0; index < 5; index++) {
//         fs.writeFile(path.join(__dirname,"mydir2",`file${index}.js`), "Hello World", (error) => {
//             if(error) {
//                 return console.log(error);
//             }
//             console.log(`file is created`);
//         })
//     }
// }   

// createFiles();


// fs.readdir(path.join(__dirname, "mydir2"), (error, files) => {
//     if(error) {
//         return console.log(error);
//     }

//     files.map((file) => {
//         fs.unlink(path.join(__dirname, "mydir2", file), (error) => {
//             if(error) {
//                 return console.log(error);
//             }
//             console.log("file removed successfuly");
//         })
//     })
// })

// fs.rmdir(path.join(__dirname, "mydir"), (error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log("removed directory successfully");
// })

// fs.unlink(path.resolve(".", "file1.js"), (error) => {
//     if(error) {
//         return console.log(error);

//     }

//     console.log("file removed successfully");
// })