//popular array utility methods

//filter - hof

const data = [{"id":1,"card_number":"5602221055053843723","card_type":"china-unionpay","issue_date":"5/25/2021","salt":"x6ZHoS0t9vIU","phone":"339-555-5239"},
{"id":2,"card_number":"3547469136425635","card_type":"jcb","issue_date":"12/18/2021","salt":"FVOUIk","phone":"847-313-1289"},
{"id":3,"card_number":"5610480363247475108","card_type":"china-unionpay","issue_date":"5/7/2021","salt":"jBQThr","phone":"348-326-7873"},
{"id":4,"card_number":"374283660946674","card_type":"americanexpress","issue_date":"1/13/2021","salt":"n25JXsxzYr","phone":"599-331-8099"},
{"id":5,"card_number":"67090853951061268","card_type":"laser","issue_date":"3/18/2021","salt":"Yy5rjSJw","phone":"850-191-9906"},
{"id":6,"card_number":"560221984712769463","card_type":"china-unionpay","issue_date":"6/29/2021","salt":"VyyrJbUhV60","phone":"683-417-5044"},
{"id":7,"card_number":"3589433562357794","card_type":"jcb","issue_date":"11/16/2021","salt":"9M3zon","phone":"634-798-7829"},
{"id":8,"card_number":"5602255897698404","card_type":"china-unionpay","issue_date":"1/1/2021","salt":"YIMQMW","phone":"228-796-2347"},
{"id":9,"card_number":"3534352248361143","card_type":"jcb","issue_date":"4/28/2021","salt":"zj8NhPuUe4I","phone":"228-796-2347"},
{"id":10,"card_number":"4026933464803521","card_type":"visa-electron","issue_date":"10/1/2021","salt":"cAsGiHMFTPU","phone":"372-887-5974"}]

//filter as name suggests filters objects based on the testing function we are passing

//filter cards which are issued in the month of january

// const janCard = data.filter((object) => {
//     let issuedDate = new Date(object['issue_date']);

//     if(issuedDate.getMonth() == 0) { //0 represents first month
//         return true // based on true the objects gets filtered
//     }
// })

// console.log(janCard);

//filtering objects based on the card type
// const jcbCard = data.filter((object) => {
//     return object['card_type'] == 'jcb';
// })
// console.log(jcbCard);

// const modifiedArray = data.filter((object, index, array) => {
//     array['country'] = 'india';
//     return 1;
// })

// console.log(modifiedArray);
// console.log(data);


//************* find */
//return the value as soon as it finds truthy value
//returns undefined if truthy value is not returning
//find basically logs first value which returns the truthy value

// let arr1 = [1,2,3,4,5,6,7,8];
// let returnedValue = arr1.find((number) => {
//     return (number%2 === 0)
// })

// console.log(returnedValue); // 2

// let arr1 = [1,2,3,4,5,6];
// let returnedValue = arr1.find((number) => {
//     return (number % 10 === 0);
// })

// console.log(returnedValue); //undefined, did not find any value which returns truthy value

//************* find index */
//similar to the find function, but instead of returning the value if returns the index of that value
//returns -1 incase if it does not find anything which satisfies the testing function


//let arr1 = [1,2,3,4,6,25];
// let index = arr1.findIndex((number) => {
//     return (number % 5 === 0);
// })

// console.log(index); //5

// let index = arr1.findIndex((number) => {
//     return (number%7 === 0);
// })

// console.log(index); //-1

//********************flat */
//flatens the nested array to one dimensional array
//takes depth as parameter, indicating how depth we have to flat, defaults to one
// let arr1 = [1,2,3,4,5,[6]];
// let flatArray = arr1.flat();
// console.log(flatArray);

// let arr1 = [[1], [[[3]]], [4], 5];
// let flatArray = arr1.flat(Infinity);

// console.log(flatArray); //passing infinity as depth it flattens the array to one dimensional no matter how nested the array is.


//*****************ForEach */;
//just like normal loop, returns nothing but iterates over the elements in array

// let arr1 = [1,2,3,3];

// arr1.forEach((number) => {
//     console.log(number * 0);
// })

//***************Includes */;
//returns boolean if it includes in array else false.
//includes second parameter if you want to search from specific index

// let arr1 = [1,2,3,4,5,6];

// console.log(arr1.includes(2));//true
// console.log(arr1.includes(2,1));//true
// console.log(arr1.includes(2,2));//false

//******************IndexOf */
//similar to findIndex, instead of using testing function, it takes the value itself
//it found returns the index else returns -1
//it also takes one more parameter, to start searching from specific index
// let arr1 = [1,2,3,4,'sanith', 'tony', 'ross', 'monica', 'chandler'];
// console.log(arr1.indexOf('chandler')); //8
// console.log(arr1.indexOf('joey'))// -1


//*************Array.isArray() */
//straight forward, returns true it passed element is array else false

// let arr1 = [1];
// console.log(Array.isArray(arr1));


//***************Array join */
//uses delimiter to join the elements;
// let arr1 = ['Wind', 'Water', 'Fire'];

// console.log(arr1.join());
// console.log(arr1.join(''));
// console.log(arr1.join('-'));
// console.log(arr1.join('+'));


// let arr1 = [1,2,3,4,5];
// let iterator = arr1.keys();
// console.log(iterator);


//*****************LastIndexOf */
//similar to IndexOf, instead of returning first index it returns last index
//compares using strict equal to.
let numbers = [2, 5, 9, 2];
console.log(numbers.lastIndexOf(2, 10));

