//Higher Order Functions Practice

let array1 = [10,18,4,3,2,1];
array1.sort(); //without function sorting consideres elements as strings
//console.log(array1);

let arr2 = ["captain", "america",'Captain', 'America', 'Iron', "Man"];
arr2.sort(); // block letters given first priority without passing any function
//console.log(arr2);

let arr3 = [4, 2, 5, 1,'sanith'];
arr3.sort((num1, num2) => {
    return num1 - num2; 
})
/*
// if positive value which indicates a is greater than b, so it sorts b first and a next.
By defaults sort function will arrange elements in ascending order.
*/
//console.log(arr3);

let arr4 = ['apple', 'Apple', 'zulu', "Zebra", "charlie", "Cat"]

arr4.sort((character1, character2) => {
    //return character1 - character2; //they wont return numbers

    if(character1 > character2) { //comparing unicodes apple, a has around 97, A is around 65
        return 1;
    }
    if(character1 < character2) {//if not above, now number < 0, char2 will come first and char 2
        return -1;
    }

    return 0
})

//console.log(arr4); //[ 'Apple', 'Cat', 'Zebra', 'apple', 'charlie', 'zulu' ]
//strings wise unicodes of block letters will be less than small letters.


const data = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

// 1. Find all people who are Agender
// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
// 3. Find the sum of all the second components of the ip addresses.
// 3. Find the sum of all the fourth components of the ip addresses.
// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
// 5. Filter out all the .org emails
// 6. Calculate how many .org, .au, .com emails are there
// 7. Sort the data in descending order of first name

//first sorting in ascending order, trying same method as above

data.sort((object1, object2) => {//sorting works with case sensitivity
    let name1 = object1['first_name'];
    let name2 = object2['first_name'];

    //now comparing
    if(name1 > name2) { //comparing unicodes greaterthan will return number > 0
        return 1;
    } 
    if(name1 < name2) {//if something is less than the compared value, if we do name1 - name2, we will get number < 0
        return -1;
    }

    return 0 //what if we are comparing same values or characters so 0 to indicate no change in value
})

//console.log(data); //logging alphabetical order
//console.log(data.reverse()); // logging in reverse alphabetical order as the name itself is self explainatory


const items = [
    { name: 'Edward', value: 21 },
    { name: 'Sharpe', value: 37 },
    { name: 'And', value: 45 },
    { name: 'The', value: -12 },
    { name: 'Magnetic', value: 13 },
    { name: 'Zeros', value: 37 }
];
//sorting alphabetical order and numeric ascending order

// items.sort((num1,num2) => {
//     return num1['value'] - num2['value'] // same thing > 0, < 0, == 0
// })
//console.log(items);
//console.log(items.reverse());

items.sort((name1, name2) => {
    if(name1['name'] > name2['name']) {
        return 1; // return -1 for reverse sorting
    }
    else if(name1['name'] < name2['name']) {
        return -1; // return 1 for reverse sorting
    }
    else {
        return 0;
    }
})
//console.log(items); // sorts alphabetically
//console.log(items.reverse());

const arr5 = [31,12,15,16,16,18,19,20,22,24]
// removing duplicates using HOF

const result5 = arr5.filter((num1, num2) => {//num2 is index comparing index with index
    return arr5.indexOf(num1) == num2; 
})
//console.log(result5);


//slice and splice

let arr6 = [1,2,3,4,5,10,11,[11,12,15]];
//console.log(arr6.slice(-1));

//splice
let arr7 = [1,2,3,4,5,6,7,8,9,10];
arr7.splice(0,2,3); //starts from index 0, remove two elements replace the with 3
//console.log(arr7)