//popular array utility methods

//Array.prototype.concat()
//It merges two or more arrays, it does not change the original array rather return new merged array

// let arr1 = [1,2,3,4,5];
// let arr2 = [5,6,7,8,{name : 'sanith'}];

// let arr3 = arr1.concat(arr2);
// console.log(arr3); // [ 1, 2, 3, 4, 5, 5, 6, 7, 8, { name: 'sanith' } ]

//concatenating two arays
// let arr1 = [1,2,3];
// let arr2 = [4,5,6];
// let arr3 = [7,8,9];

// let arr4 = arr1.concat(arr2, arr3);
// console.log(arr4); //[ 1, 2, 3, 4, 5, 6, 7, 8, 9]

//we can include values as well, they will get added directly if it is array it will concatenate
// let arr1 = [1,2,3];
// let arr2 = arr1.concat(1,[[2]],[3,4]); // flattens one dimensional and add them to the array
// console.log(arr2);


//Array.prototype.copyWithin()
//self explainatory, for copying elements within the array without changing its length
//it changes the original array

// let arr1 = [1,2,3,4,5,6,7];
// let arr2 = arr1.copyWithin(0,6,7); //target value at what index you want to copy, start and end, copy what elemenst start index and end index

// console.log(arr2);

// let arr1 = ['b', 'a', 'c', 'd'];
// console.log(arr1.copyWithin(1, 0, 1)); //[ 'b', 'b', 'c', 'd' ]

// let arr1 = [1,2,3,4,5,6];
// console.log(arr1.copyWithin(-1, 0, 1));//negative index starts from last 

// let arr1 = [1,2,3,4,5];
// console.log(arr1.copyWithin(0, 2)) // if we dont mention the end index it picks the element at the mentioned index


//Array.prototype.entries()
//creates array iterator object with key value pairs

// let arr1 = [1,2,3];
// let iterator = arr1.entries();

// for(let index of iterator) {
//     console.log(index); //logs arrays with index and values
// }
//[ 0, 1 ]
//[ 1, 2 ]
//[ 2, 3 ]

// for(let[index, element] of iterator) {
//     console.log(`element at index ${index} is ${element}`);
// }
/* 
element at index 0 is 1
element at index 1 is 2
element at index 2 is 3
*/

//Array.prototype.every()
//checks every value with the testing functions and return boolean value
// let arr1 = [1,2,3,4,11];
// console.log(arr1.every((value) => {
//     return value < 10
// }))


//Array.prototype.fill()
//changes values at specified indexes
//it changes original array
// let arr1 = [1,2,3,4,5];
// console.log(arr1.fill(5, 0)); // fills 5 starting from index 0 to end
// console.log(arr1.fill(1,1,2));
// console.log(arr1);


//Array.prototype.filter()
//filters array based on return of boolean value from the testing function

// let multipleOfFive = [2,4,6,8,10,11,25,30].filter((value) => {
//     if(value% 5 === 0) {
//         return true; //return array which pass the condition
//     };
// })

// console.log(multipleOfFive);

//we can remove duplicates using filter

// let arr1 = [1,2,3,3,4,4,5,5,5,6,7];

// let arr2 = arr1.filter((value, index) => {
//     return arr1.indexOf(value) == index;
// })

// console.log(arr2); //[1,2,3,4,5,6,7]
//indexOf returns the index of first found value, if the index position of the current looking value and index parameter value is same only then it returns as truthy value

// let arr1 = [1,2,3,4,6,5];

// let arr2 = arr1.filter((value, index, array) => {
//     array[1] = 10;
//     return true;
// })

// console.log(arr2);
// console.log(arr1);

/*
[ 1, 10, 3, 4, 6, 5 ]
[ 1, 10, 3, 4, 6, 5 ],
array is called by reference not as by value so the changes are reflecting in both original and cloned array
*/

// let arr1 = [1,2,3,4,5,6];

// let arr2 = arr1.filter((value, index, array) => {
//     array.push(7); //effects the original array, each iteration we are pushing 7
//     return true;
// })

// console.log(arr2);
// console.log(arr1);

//Array.prototype.find()
//sends the first find element that matches the testing function, else it returns undefined

// let arr1 = [1,1,1,1,1,1,1,1,2,4,6,8,20];

// let multiplesOfTwo = arr1.find((value) => {
//     return value % 2 == 0;
// })

// console.log(multiplesOfTwo);