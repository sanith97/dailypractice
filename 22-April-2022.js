// What is semver. What does it mean? How does it work?
// What do the symbols in the version number of a dependency in package.json mean?
// What are the different types of licences that are available and how do they differ?
// What are npm scripts? 
// What is the purpose of package-lock.json?
// What is the difference between a dependency and a devDependency?
// Why do we not use --save with npm install anymore?
// Why is committing node_modules bad?
// What is the difference between a globally installed module and a locally installed module?
// How do you tell the difference between an inbuilt module and an external module?
// Why is using git commit -m bad?

// What is semver. What does it mean? How does it work?
// "dependencies": {
//     "loadash": "^1.0.0"
//   }

//semver-semantic versioning. 
//npm uses it combined with rule symbols to record what versions of package
//satisfies dependencies.
//easy to know which version is installed and compatiable versions it update the package.
//npm by default installs the latest updated package if version is not provided explicitly.

// it is used to differentiate major breaking changes, minor updates and patches.
//1.0.0
//major version will only change if there are major changes in the code.
//minor version backwards compatible changes.
//security and patch update, bug fixes

//NPM ecosystem uses semver to track what kinds of changes each version includes.
//When ever the author publishes the updated version of package npm asks to mention the update in semver format
//When an NPM package is updated, the version number is updated.
// The npm cli uses semver to understand how to satisfy the dependency requirements.



//Symbols in the version number control what versions are allowed to install.
//Rule symbols tells npm how to handle future updates

//"loadash": "^1.0.0", whenever you install a package it installs with a caret(^) prefix.
//^ - caret indicates anything within this major update.
//so from 1.0.0 - 2.0.0
//^4.3.2 - anything from 4.3.2 - 5.0.0 - version with non-breakable changes

//other rule symbols - [~, >, >=, <, <=, ||];

//~-telda, allows only patch updates not major or minor updates.
//>, <, >=, <=, straightforward, greaterthan version, lessthan version.
// || - or symbol allow us to combine rule symbols

//DEPENDENCY-DRIFT
//since npm install can install a range of versions, differences in package versions can cause difficulty while debugging.
//for example, you are working on app and using package with version
//^2.3.8, by the time you are deploying it updated to 2.8.8, resulting in 
//changes to your code functionality.

//To prevent this Package-lock.json is introduced.
//it records the exact versions of dependencies and sub-deendencies.
//package-lock.json tells npm install what exact versions to install for each package,
// and its underlying dependencies.

//creates a reproducable snapshot of all of your dependencies.
// until you specifically tell npm to install updated package it wont do it.
//it will check into version control to prevent dependency drift.

