//************************different datatypes in JavaScript

//- string - sequence of chars enclosed in single or double quotes
//console.log(typeof "hello");
// - Boolean either true or false
// - Number, int and float

// - undefined, varible declared but not initialised.
// let a;
// console.log(a);//undefined

// - Null, represents nothing

// - Big int, values between (2^23 -1 < big int < 2 ^23 + 1)
// - Symbol, unique and immutable

// - Object, key value pairs, except this all other data types comes under primitive data
// objects, arrays, functions comes under reference type.



// ********************************** let, var, const

// let - block scoped, where ever we use blocks it works for it, functions, loops, etc
// {
//     let a = 5;
//     console.log(a);//5
// }

// console.log(a);//reference error;

//if any variable assigned with let it allows you to change value
// let a = 5;
// a = 10;
// console.log(a); // 10


//var - function scope
// function hello() {
//     var name = 'sanith';
//     console.log(name);
// }
// hello();
// console.log(name); //reference error, not defined

// {
//     name = 'sanith';
//     console.log(name);//sanith
// }

// console.log(name); //sanith

// if we assign value without declaring it, it picks var by default

// name = "sanith";
// console.log(name);//logs sanith, coz it declating not provided, it picks var
// var name = 'sanith';
// console.log(name);
// name = "kumar";
// console.log(name);


//const - Block scoped, another way of declaring variables, once assigned cannot be changed.
// const name = 'sanith';
// name = 'garipally';
// console.log(name); //Type Error, assignment to constant variable

// {
//     const name = "sanith";
// }
// console.log(name);


// ********************************* why we must not use var

//var - global/function scope

// function addition() {
//     var number1 = 10;
//     var number2 = 20;

//     if(number1 > number2) {
//         number1 = 1;
//     }
//     else {
//         number2 = 1;
//     }
//     number2 = 10;
//     //since they are function scopes, it can be accessible through out the file and any unnecessary change made to it 
//     //will be hard to debug because consider we have 10000 lines of code and you have declared it in global scope, if the value got changed it will be hard to find at which line it changed.
//     console.log(number1);
//     console.log(number2);
// }

// addition();

// var value1 = 10;

// function addition() {
//     var value2 = 10
//     console.log(value1 + value2);
// }

// addition();

// var value1 = 10;

// function printme() {
//     value1 = 20;
//     console.log(value1);
// }

// // 
// console.log(value1);//10
// printme();//20


// ****************************** why using global variables is bad
//it will be accessible through out the file and any bugs related to that issue will be hard to debug.


// ********************************* truthy and falsy values
//truthy - in boolean context any value that is comes under true category considered as truthy value.
//[], {}, strings(not an empty string), numbers, true, 

//false - in boolean context, any value that comes under false category considered as false value
//null, undefined,NaN, false, 0, -0, "".


// const promise1 = new Promise((resolve, reject) => {
//     resolve('promise 1');
// });
// promise1.then(console.log());

// const promise2 = new Promise((resolve, reject) => {
//     resolve('promise 2');
// });
// promise2.then(console.log); //waits till it resolved and pass the value implicitly tp the console.log function

// const promise3 = new Promise((resolve, reject) => {
//     resolve('promise 3');
// });
// promise3.then(v => console.log(v));
// waits till it gets resolved and can pass the arguments explicitly


// const promise = new Promise((fulfill, reject) => {
//     setTimeout(() =>
//     reject(Error('REJECTED!')) , 300)
// })

// function onReject(error) {
//     console.log(error.message);
// }

// promise.then(null, onReject)


// const promise = new Promise((fulfill, reject) => {
//     fulfill("I FIRED");
//     reject(Error("I DID NOT FIRE"));
// })

// function onRejected(error) {
//     console.log(error.message);
// }

// promise.then(console.log, onRejected);


// const promise = new Promise((fulfill, reject) => {
//     fulfill('PROMISE VALUE');
// })

// promise.then(console.log);

// console.log("MAIN PROGRAM");


// const promise = new Promise((fulfill, reject) => {
//     reject(new Error("secret value"));
// })
// promise.catch((error) => {
//     console.log(error.message);
// })

// const promise1 = Promise.resolve("SECRET VALUE");
// promise1.then(console.log); 

// const promise2 = Promise.reject(new Error ("SECRET VALUE"));

// promise2.catch((error) => {
//     console.log(error.message);
// });


// 'use strict';

// var message;
// var promise;

// function randomBytes(n) {
//     return (Math.random() * Math.pow(256, n) | 0).toString(16);
// }

// message =
//     'A fatal exception ' + randomBytes(1) + ' has occurred at ' +
//     randomBytes(2) + ':' + randomBytes(4) + '. Your system\nwill be ' +
//     'terminated in 3 seconds.';

// promise = Promise.reject(new Error(message));

// promise.catch(function (err) {
//     var i = 3;

//     process.stderr.write(err.message);

//     setTimeout(function boom() {
//         process.stderr.write('\rwill be terminated in ' + (--i) + ' seconds.');
//         if (!i) {
//             process.stderr.write('\n..... . . . boom . . . .....\n');
//         } else {
//             setTimeout(boom, 1000);
//         }
//     }, 1000);
// });



//functions are already defined
// first().then((res) => {;
//     return second(res);
// }).then(console.log)


// function attachTitle(value) {
//     return (`DR. ${value}`);
// }

// const promise = Promise.resolve('MANHATTAN')

// promise.then(attachTitle)
//  .then(console.log)


// function parsePromised(json) {
//     return new Promise(function (fulfill, reject) {
//         try {
//             fulfill(JSON.parse(json));
//         } catch (e) {
//             reject(e);
//         }
//     });
// }


// parsePromised(process.argv[2])
//     .then(null, (error) => console.log(error.message));


// function alwaysThrows() {
//     throw Error("OH NOES");
// }

// function iterate(param1) {
//     console.log(param1);
//     return param1 + 1;
// }

// const promise = Promise.resolve(iterate);

// promise.then((param1) => {
//     for(let index = 1; index < 11; index++){
//         if(index < 6) {
//             param1(index);
//         }
//         else {
//             alwaysThrows();
//         }
//     }
// }).then(null, (error) => {
//     alwaysThrows(error);
// }).then(null, (error) => {
//     console.log(error.message);
// })

// function iterate(num) {
//     console.log(num);
//     return num + 1;
//   }

//   function alwaysThrows() {
//     throw new Error('OH NOES');
//   }

//   function onReject(error) {
//     console.log(error.message);
//   }

//   Promise.resolve(iterate(1))
//   .then(iterate)
//   .then(iterate)
//   .then(iterate)
//   .then(iterate)
//   .then(alwaysThrows)
//   .then(iterate)
//   .then(iterate)
//   .then(iterate)
//   .then(iterate)
//   .then(iterate)
//   .catch(onReject);


// function all(promise1, promise2) {
//     let counter = 0;
//     let arr1 = [];

//     return new Promise((fulfill, reject) => {
//         promise1.then((value1) => {
//             counter++;
//             arr1[0] = value1;
//             if (counter >= 2) {
//                 fulfill(arr1);
//             }
//         })
//         promise2.then((value2) => {
//             counter++;
//             arr1[1] = value2;
//             if (counter >= 2) {
//                 fulfill(arr1);
//             }
//         })
//     })
// }


// all(getPromise1(), getPromise2())
//     .then(console.log)

// const path = require('path');
// const fs = require('fs');
//const qhttp = require('q-io/http');

// qhttp.read("https://tools.learningcontainer.com/sample-json.json")
//  .then((json) => {
//      console.log(JSON.parse(json))
//  })
//  .then(null, console.error)
//  .done()

// qhttp.read("http://localhost:7000/")
//  .then((id) => {
//      return qhttp.read("http://localhost:7001/" + id);
//  })
//  .then((json) => {
//      console.log(JSON.parse(json));
//  })
//  .then(null, console.error)
//  .done()


// const promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve([1,2,3,4,5]);
//     },3000)
// })

// promise.then((message) => {
//     console.log(message);
// })
// .catch((error) => {
//     console.log(error.message);
// })

// promise.then((message)=> console.log(message[0]));

// const promise = Promise.resolve(1997);

// const promise1 = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         promise.then((value) => console.log(value));
//     }, 3000)
// })

//promise1.then((value) => console.log(value));

// const status = response => {
//     if (response.status >= 200 && response.status < 300) {
//         return Promise.resolve(response)
//     }
//     return Promise.reject(new Error(response.statusText))
// }

// const json = response => response.json()

// fetch('./output.json')
//     .then(status)    // note that the `status` function is actually **called** here, and that it **returns a promise***
//     .then(json)      // likewise, the only difference here is that the `json` function here returns a promise that resolves with `data`
//     .then(data => {  // ... which is why `data` shows up here as the first parameter to the anonymous function
//         console.log('Request succeeded with JSON response', data)
//     })
//     .catch(error => {
//         console.log('Request failed', error)
//     })

// console.log("Before...");
// setTimeout(() => {
//     console.log('It took 300ms to get there.');
// });
// console.log("After...");


//***************************TO BE COMPLETED.
// * function hoisting
// * what happens when a function does not have a return statement
// * different ways of declaring a function
// * pass by reference and pass by value
// * different types of for loops - for with numbers, for..in, for..of, forEach
// * searching mdn (mozilla developer network)
// * popular array utility methods
// * popular string utility methods
// * popular object utility methods
// * when to use forEach, when to use array utility methods like map, filter, reduce
// * immutable and mutable methods
// * error handling (try catch)
// * throwing errors
// * difference between throw new Error("Error message here") and throw "Error message here
// * reading error messages and tracing issues from the stack trace when errors happen - practice this daily for 2 weeks with different examples. this is very important
// * importance of catch block
// * spread operator
// * template literals
// * default parameters
// * destructuring
// * closures
// * difference between arrow functions and regular functions
// * difference between === and ==
// * why using value === undefined is better than using !value
// * array utility methods chaining
// * difference between null and undefined
// * importing and exporting modules using require and module.exports
// * the different methods of console such as console.log, console.error, console.info and so on
// * all the best practices mentioned on the LMS - indendation, variable naming, loop variable naming and all the others
// * passing functions to other functions and invoking them on demand
// * differences between named functions and anonymous functions
// * variable number of arguments passed to functions (edited)



