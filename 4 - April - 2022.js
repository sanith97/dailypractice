//Linux revision

//pipes - output of one command is passed as input of another,basically we can chain the commands

//using combination of pipes and sort

//consider i have a practice file - practice.txt with some
//content int

//cat practice.txt | sort
//sort the sentences in alphabetical order

//cat practice.txt | sort -r
//sort the sentences in reverse alphabetical order

//if practice.txt includes numbers
//cat practice.txt | sort -n
//sort the numbers in ascending order

//cat practice.txt | sort -nr
//sort numbers in descending order

//good thing about sort command, it will not hamper the
//original file



//grep command
//global search for regular expression and print out
//basically it will search the document based on the 
//instructions we provide and prints o/p.

//grep -i "word" practice.txt
//it searches for the "word" in practice.txt with case insensitivity


//grep -c "unix" practice.txt
// it searches for the word "unix" and prints the count in
//the file practice.txt
// searching can be possible for multiple files

//consider we have 10 files and we have to find word
// "sanith" in those files, we need file names which contain that word

//grep -l "sanith" *.txt 
//list files ending with txt search for word "sanith"

//grep -w "unix" practice.txt 
//to find whole words matching with name "unix"
//but it prints the other words as well.

//to print only the matched words not the other words
//grep -o "unix" practice.txt

//to count number of lines we can use o/p of this pipe it 


//if we need to print the lines of a file that do not match
//with the word we mention

//grep -v "unix" practice.txt
//prints all the lines that does not include unix




//**********************************Async JS 
// console.log('Start');

// function loginUser(email, password) {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             console.log('Now we have the data')
//             resolve({
//                 userEmail : email
//             })
//         }, 1500);
//     })
// }

// function getVideos(email) {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve(['videos1', 'videos2', 'videos3']);
//         }, 1000);
//     })
    
// }

// function getVideoDetails(video) {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve("Title of the video")
//         })
//     })
// }

// loginUser("gsk@gmail.com", "ohyeah")
//  .then(user => getVideos(user.userEmail))
//  .then(videos => getVideoDetails(videos[0]))
//  .then(details => console.log(details))

// console.log('Finish');

// const promise = new Promise((resolve, reject) => {
//     let x = "Hello";
//     let y = "Hello1";

//     if(x == y) {
//         resolve('sanith');
//     }
//     else {
//         reject();
//     }
// })

// promise.then((user) => console.log(`Hello, My name is ${user}`))
// .catch(() => console.log("An Error Occurred"))

// const promise = new Promise((resolve, reject) => {
//     resolve("sanith");
// })

// function demo(name) {
//     console.log(`Hello ${name}`);
// }

// promise.then(demo)


//Path module delete operations

// fs.readdir(path.join(__dirname, "mydir2"), (error, files) => {
//     if(error) {
//         return console.log(error);
//     }

//     files.map((file) => {
//         fs.unlink(path.join(__dirname, "mydir2", file), (error) => {
//             if(error) {
//                 return console.log(error);
//             }
//             console.log("file removed successfuly");
//         })
//     })
// })

// fs.rmdir(path.join(__dirname, "mydir"), (error) => {
//     if(error) {
//         return console.log(error);
//     }

//     console.log("removed directory successfully");
// })

// fs.unlink(path.resolve(".", "file1.js"), (error) => {
//     if(error) {
//         return console.log(error);

//     }

//     console.log("file removed successfully");
// })

//path.resolve - takes the absolute path relative to the current
//working directory

//path.join(__dirname) - takes abs path of the folder with
//reference to the file where it is executing