// let data = [
//     {
//       Year: "2018",
//       Ranking: "1",
//       Name: "Osteria Francescana",
//       City: "Modena",
//       Country: "Italy",
//       Latitude: "44.6431",
//       Longitude: "10.9341",
//     },
//     {
//       Year: "2018",
//       Ranking: "2",
//       Name: "El Celler de Can Roca",
//       City: "Girona",
//       Country: "Spain",
//       Latitude: "41.9808",
//       Longitude: "2.8187",
//     },
//     {
//       Year: "2018",
//       Ranking: "3",
//       Name: "Mirazur",
//       City: "Menton",
//       Country: "France",
//       Latitude: "43.7747",
//       Longitude: "7.5046",
//     },
//     {
//       Year: "2018",
//       Ranking: "4",
//       Name: "Eleven Madison Park",
//       City: "New York",
//       Country: "USA",
//       Latitude: "40.7146",
//       Longitude: "-74.0071",
//     },
//     {
//       Year: "2018",
//       Ranking: "5",
//       Name: "Gaggan",
//       City: "Bangkok",
//       Country: "Thailand",
//       Latitude: "13.75",
//       Longitude: "100.516",
//     },
//     {
//       Year: "2018",
//       Ranking: "6",
//       Name: "Central",
//       City: "Lima",
//       Country: "Peru",
//       Latitude: "-12.043",
//       Longitude: "-77.028",
//     },
//     {
//       Year: "2018",
//       Ranking: "7",
//       Name: "Maido",
//       City: "Lima",
//       Country: "Peru",
//       Latitude: "-12.043",
//       Longitude: "-77.028",
//     },
//     {
//       Year: "2018",
//       Ranking: "8",
//       Name: "Arpège",
//       City: "Paris",
//       Country: "France",
//       Latitude: "48.8569",
//       Longitude: "2.3412",
//     },
//   ];

//reduce
//finding number of cities per country  

// let citiesPerCountry = data.reduce((totalNumber, currentObject) => {
//     let count = 1;
//     if((currentObject['Country'] in totalNumber) && (!(totalNumber[currentObject['Country']].includes(currentObject['City'])))) {
//         totalNumber[currentObject['Country']].push(currentObject['City']);
//         totalNumber[currentObject['Country']][0] += count;

//     }
//     else {
//         totalNumber[currentObject['Country']] = [];
//         totalNumber[currentObject['Country']].push(count);
//         totalNumber[currentObject['Country']].push(currentObject['City']);
//         // console.log(totalNumber[currentObject['Country']]);
//     }
//     return totalNumber;
// }, {});

// console.log(citiesPerCountry);

//sorting objects based on latitudes

// let latitudeArray = data.sort((object1, object2) => {
//     return (Number(object1['Latitude']) - Number(object2['Latitude']));
// })

// console.log(latitudeArray);

//sorting works in this manner, if value after subtraction is greaterthan 1, then the values will be reversed
//it lessthan 1 then it will not reverse and equal to 0 will not change the places.

//filter all the negative longitudes

// let negativeLongitudes = data.filter((object) => {
//     return (Number(object['Longitude']) < 0);
// })

// console.log(negativeLongitudes);

let data1 = [{"id":1,"color":"Indigo","material":"Plexiglass","quantity":49,"cost":"$0.56"},
{"id":2,"color":"Mauv","material":"Stone","quantity":60,"cost":"$2.76"},
{"id":3,"color":"Aquamarine","material":"Glass","quantity":82,"cost":"$9.03"},
{"id":4,"color":"Puce","material":"Stone","quantity":69,"cost":"$0.42"},
{"id":5,"color":"Turquoise","material":"Plastic","quantity":15,"cost":"$4.52"},
{"id":6,"color":"Violet","material":"Vinyl","quantity":57,"cost":"$7.66"},
{"id":7,"color":"Indigo","material":"Glass","quantity":13,"cost":"$6.23"},
{"id":8,"color":"Fuscia","material":"Plastic","quantity":48,"cost":"$0.47"},
{"id":9,"color":"Yellow","material":"Stone","quantity":1,"cost":"$2.76"},
{"id":10,"color":"Indigo","material":"Rubber","quantity":87,"cost":"$1.06"},
{"id":11,"color":"Violet","material":"Granite","quantity":39,"cost":"$3.37"},
{"id":12,"color":"Yellow","material":"Wood","quantity":33,"cost":"$7.69"},
{"id":13,"color":"Red","material":"Brass","quantity":54,"cost":"$4.28"},
{"id":14,"color":"Teal","material":"Aluminum","quantity":56,"cost":"$2.78"},
{"id":15,"color":"Puce","material":"Granite","quantity":32,"cost":"$1.09"},
{"id":16,"color":"Puce","material":"Wood","quantity":13,"cost":"$9.61"},
{"id":17,"color":"Violet","material":"Vinyl","quantity":51,"cost":"$3.10"},
{"id":18,"color":"Violet","material":"Stone","quantity":24,"cost":"$0.15"},
{"id":19,"color":"Fuscia","material":"Vinyl","quantity":91,"cost":"$8.22"},
{"id":20,"color":"Aquamarine","material":"Rubber","quantity":15,"cost":"$2.28"},
{"id":21,"color":"Crimson","material":"Glass","quantity":88,"cost":"$3.33"},
{"id":22,"color":"Orange","material":"Wood","quantity":21,"cost":"$5.07"},
{"id":23,"color":"Orange","material":"Stone","quantity":56,"cost":"$2.13"},
{"id":24,"color":"Puce","material":"Steel","quantity":27,"cost":"$0.68"},
{"id":25,"color":"Teal","material":"Rubber","quantity":44,"cost":"$2.46"},
{"id":26,"color":"Teal","material":"Glass","quantity":64,"cost":"$8.74"},
{"id":27,"color":"Indigo","material":"Plexiglass","quantity":1,"cost":"$1.05"},
{"id":28,"color":"Orange","material":"Glass","quantity":58,"cost":"$1.32"},
{"id":29,"color":"Aquamarine","material":"Stone","quantity":27,"cost":"$1.07"},
{"id":30,"color":"Indigo","material":"Glass","quantity":89,"cost":"$5.82"}];

//filter all plastic materials

// let plasticMaterials = data1.filter((object) => {
//     return (object['material'] == 'Plastic');
// })

// console.log(plasticMaterials);

//total cost of the data

// let sumOfData = data1.reduce((totalSum, currentObject) => {
//     return totalSum + Number(currentObject['cost'].slice(1));
// }, 0);

// console.log(sumOfData);

//find the total quantities based on material;

// let totalMaterial = data1.reduce((totalSum, currentObject) => {
//     if(currentObject['material'] in totalSum) {
//         totalSum[currentObject['material']] += Number(currentObject['quantity']);
//     } else {
//         totalSum[currentObject['material']] = Number(currentObject['quantity']);
//     }
//     return totalSum;
// }, {});

// console.log(totalMaterial);

//quantities based on color

let totalQuantitiesBasedOnColor = data1.reduce((totalSum, currentObject) => {
    if(currentObject['color'] in totalSum) {
        totalSum[currentObject['color']] += Number(currentObject['quantity'])
    } else {
        totalSum[currentObject['color']] = Number(currentObject['quantity']);
    }
    return totalSum;
}, {});

console.log(totalQuantitiesBasedOnColor);