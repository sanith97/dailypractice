//String Slice
//extracts the required part and returns it without modifying the original string.

//Takes two arguments, start and end index, where end index is not included.
//Works with negative indexes as well.

let str1 = "Hello, My name is sanith and i am from kamareddy."
let slicedString = str1.slice(0,5)
// console.log(slicedString);
// console.log(str1.slice(-11, -1))

//useful when we need only certain part of the string and we know what string it is.

let money = [{
    account: "$150"},
    {
    account: "$178.64"},
    {
    account: "$230.78"
}]

let moneyValue = money.map((currency) => {
    return Number(currency['account'].slice(1));
})
// console.log(moneyValue); [ 150, 178.64, 230.78 ]

//Array.split
//splits the sub string into ordered list using the delimiters provided as the first parameter

let data = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

// 1. Find all people who are Agender
// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
// 3. Find the sum of all the second components of the ip addresses.
// 3. Find the sum of all the fourth components of the ip addresses.
// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
// 5. Filter out all the .org emails
// 6. Calculate how many .org, .au, .com emails are there
// 7. Sort the data in descending order of first name

//spliting TP addresses into their components

let ipComponents = data.map((object) => {
    object['ip_address'] = object['ip_address'].split(".");
    return object;
})

// console.log(ipComponents);

//String.StartsWith
//returns boolean value, takes two parameters, one string and index to start searching from, defaults to zero.

let str2 = "Hello, My name is sanith. I am from kamareddy, located in telangana."
// console.log(str2.startsWith("Hell"));
// console.log(str2.startsWith("sanith", 18))//true

//to lower and uppercase
let name1 = "sanITh KumAR";
// console.log(name1.toLowerCase());
// console.log(name1.toUpperCase());

//to string

let number = 12345;
// console.log(number.toString());

// console.log([1,2,3,4].toString());//1,2,3,4
// let object = {name:"sanith", designation: "Software Engineer"};
// console.log(object.toString());//[object, object]
// console.log(typeof object); no change for objects.

//trim - removes white spaces

let myName = "     sanith Kumar             "
let trimName = myName.trim();
console.log(trimName); //returns the trim name;

//trim end
// console.log(myName.trimEnd());
//removes white space at the end of the string.

// console.log(myName.trimStart());
//trims only in the start.


