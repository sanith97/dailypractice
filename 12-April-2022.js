//import fetch from 'node-fetch';

// const fetchPromise = fetch('https://mdn.github.io/learning-area/javascript/apis/fetching-data/can-store/products.json');

// console.log(fetchPromise);

// fetchPromise.then( response => {
//   if(!response.ok) {
//     throw new Error(`HTTP Error: ${response.status}`);
//   }
//   return response.json();
// })
// .then((json) => {
//   console.log(json[0].name);
// });

// console.log("Started request...");

// const fetchPromise = fetch('bad-scheme://mdn.github.io/learning-area/javascript/apis/fetching-data/can-store/products.json');

// fetchPromise
//   .then(response => {
//     if (!response.ok) {
//       throw new Error(`HTTP error: ${response.status}`);
//     }
//     return response.json();
//   })
//   .then(json => {
//     console.log(json[0].name);
//   })
//   .catch((error) => {
//     console.error(`Could not get products: ${error}`);
//   });

//Three promises dont depend on eachother,
const fetchPromise1 = fetch('https://mdn.github.io/learning-area/javascript/apis/fetching-data/can-store/products.json');
const fetchPromise2 = fetch('https://mdn.github.io/learning-area/javascript/apis/fetching-data/can-store/not-found');
const fetchPromise3 = fetch('https://mdn.github.io/learning-area/javascript/oojs/json/superheroes.json');

// Promise.any([fetchPromise1, fetchPromise2, fetchPromise3])//taking array of promises
//   .then( responses => { //returns single promise containing array of promises containing the same order of the requests
//     // for (const response of responses) {
//     //   console.log(`${response.url}: \n${response.status}`);
//     // }
//     console.log(`${responses.url}: \n${responses.status}`);

//   })
//   .catch( error => {
//     console.error(`Failed to fetch: ${error}`)//Failed to fetch: AggregateError: All promises were rejected
//   });

// const promise1 = Promise.reject(new Error("Error!"));
// const promise2 = new Promise((resolve) => {
//   setTimeout(() =>{
//     resolve("Waited for 2 seconds to fulfill");
//   }, 2000);
// });
// const promise3 = new Promise((resolve) => {
//   setTimeout(() => {
//     resolve("Waited 3 seconds to fullfill");
//   }, 3000);
// })

// Promise.any([promise1, promise2, promise3])
//   .then((value) => {
//     console.log(value);
//   })
//   .catch((error) => {
//     console.log(error);
//   })

//Promise.allSettled() //returns array of objects
// Promise.allSettled([fetchPromise1, fetchPromise2, fetchPromise3])//taking array of promises
//   .then( responses => { //returns single promise containing array of promises containing the same order of the requests
//     for (const response of responses) {
//       console.log(`${response.status}`);
//     }
//   })
//   .catch( errors => {
//     console.log(errors.response);
//   });

// function catchError(error) {
//   console.log(error);
// }

// const promise1 = new Promise((resolve, reject) => {
//   throw 'Oops! Internet not connected.'
// })

// promise1
//   .then((value) => {
//     console.log(value);
//   })
//   .catch((error) => {
//     return error;
//   })
//   .then(catchError);

// const p1 = new Promise(function(resolve, reject) {
//   resolve('Success');
// });

// p1.then(function(value) {
//   console.log(value); // "Success!"
//   throw new Error('oh, no!');
// }).catch(function(e) {
//   console.error(e.message); // "oh, no!"
//   throw new Error("Connection Error!")
// }).then(function(){
//   console.log('after a catch the chain is restored');
// }, function () {
//   console.log('Not fired due to the catch');
// });

// The following behaves the same as above
// p1.then(function(value) {
//   console.log(value); // "Success!"
//   return Promise.reject('oh, no!');
// }).catch(function(e) {
//   console.error(e); // "oh, no!"
// }).then(function(){
//   console.log('after a catch the chain is restored');
// }, function () {
//   console.log('Not fired due to the catch');
// });


//GOTCHAS while throwing errors

// const promise1 = new Promise((resolve, reject) => {
//   throw new Error('Oops! Internet not connected');
// })

// promise1
//   .catch((error) => {
//     console.log(error.message);
//   })

// const promise2 = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     throw new Error("Error thrown inside async function");
//   }, 2000);
// })

// promise2
//   .catch((error) => {
//     console.log(error.message);
//   })

//erros thrown after resolve will be silenced

// const p1 = new Promise((resolve, reject) => {
//   resolve();
//   throw new Error("Oh no!");
// })

// p1
//   .catch((error)=> {
//     console.log(error.message);
//   })

//create a promise which will not call onreject

// const p1 = new Promise((resolve, reject) => {
//   resolve("Yayy! Promise Resolved");

// });
// console.log(p1);
// p1
//   .then((resolveValue) => {
//     console.log(resolveValue);
//   }, (rejectedValue) => {
//     console.log(rejectedValue);
//   })

// const p2 = p1.catch((reason) => {
//   console.error("catch p1");
//   console.error(reason);
// })

// console.log(p2);
// p2
//   .then((value) => {
//     console.log("Next Promise fulfilled");
//     console.log(value);
//   }, (reason) => {
//     console.log("Next promise onRejected");
//     console.log(reason);
//   })

// function isGreater() {
//   return new Promise((resolve, reject) => {
//     if(Math.random() > 0.3) {
//       resolve("The generated number is greater than 0.3");
//     }
//     else {
//       reject(new Error("The generated number is less than 0.3"));
//     }
//   })
// }

// isGreater()
//   .then(console.log)
//   .catch((error) => console.log(error.message))
//   .finally(() => console.log("************Thank you************\n************Visit Again.************"));

// const promise1 = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     resolve("This is promise1 and it took minimun of 2 seconds for resolving")
//   }, 2000);
// });

// const promise2 = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     reject("This is promise2 and it took minimum one second for resolving")
//   }, 1000);
// })

// const promise3 = Promise.resolve("Yaay! Promise Resolved.")
// Promise.race([promise1,promise2,promise3])
//   .then((value) => {
//     console.log(value);
//   },(value) => {
//     console.log(value);
//   });

// const resolvedPromises = [Promise.resolve(1), Promise.resolve(19), Promise.resolve(97)];

// const p = Promise.race(resolvedPromises)
// console.log(p);

// setTimeout(() => {
//   console.log('The stack is now empty');
//   console.log(p);
// })


// const promisesArray1 = [Promise.race([]), Promise.resolve(100), "Hello"];
// const promisesArray2 = [Promise.resolve(100), "Hello", Promise.race([])];

// const p1 = Promise.race(promisesArray1);
// const p2 = Promise.race(promisesArray2);

// console.log(p1);
// console.log(p2);

// setTimeout(() => {
//   console.log("The stack is now empty");
//   console.log(p1);
//   console.log(p2);
// })

// var p1 = new Promise(function(resolve, reject) {
//   setTimeout(() => resolve('one'), 500);
// });
// var p2 = new Promise(function(resolve, reject) {
//   setTimeout(() => resolve('two'), 100);
// });

// Promise.race([p1, p2])
// .then(function(value) {
// console.log(value); // "two"
// // Both fulfill, but p2 is faster
// });